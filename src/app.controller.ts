import { Controller, Get, Post, Param, Logger, Body } from '@nestjs/common';
import { AppService } from './app.service';
import { ApiTags } from '@nestjs/swagger';

@Controller('')
@ApiTags('App')
export class AppController {
  private logger = new Logger('AppController');

  constructor(private readonly appService: AppService) { }
  @Get('send/:msg')
  getHello(@Param('msg') msg: string): any {
    this.logger.log('Adding ' + msg);
    return this.appService.getHello(msg);
  }
}
