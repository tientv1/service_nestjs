import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

// const http = require("http");

// const hostname = "127.0.0.1";
// const port = process.env.PORT || 8080;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();
  // app.use();
  const options = new DocumentBuilder()
    .setTitle('Manager example')
    .setDescription('The cats API description')
    .setVersion('1.0')
    // .addTag('Demo')
    .addBearerAuth(
      { type: 'http', scheme: 'bearer', bearerFormat: 'JWT' }
    )
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  await app.listen(process.env.PORT || 3000);
  // app.listen(port, hostname, () => {
  //   console.log(`Server running at http://${hostname}:${port}/`);
  // });
}
bootstrap();
