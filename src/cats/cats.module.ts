import { Module, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { CatService } from 'src/cats/services/cat/cat.service';
import { CatController } from 'src/cats/controller/cat/cat.controller';
import { LoggerMiddleware } from 'src/cats/middleware/logger.middleware';
import { MongooseModule } from '@nestjs/mongoose';
import { CatSchema, Cat } from 'src/cats/schema/cat.schema';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Cat.name, schema: CatSchema }])
    ],
    providers: [CatService],
    controllers: [CatController]
})
export class CatsModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(LoggerMiddleware)
            // bỏ qua
            .exclude(
                { path: 'cat', method: RequestMethod.POST },
                { path: 'cat', method: RequestMethod.PUT },
                'cat/(.*)'
            )
            // áp dụng 
            .forRoutes(CatController, { path: 'cat', method: RequestMethod.GET });
    }
}
