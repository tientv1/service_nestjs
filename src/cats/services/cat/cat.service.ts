import { Injectable } from '@nestjs/common';
// import { Cat } from 'src/cats/models/cat.model';
import { cloneDeep } from 'lodash';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Cat } from 'src/cats/schema/cat.schema';

@Injectable()
export class CatService {

    constructor(
        @InjectModel(Cat.name) private readonly catRepository: Model<Cat>
    ) {
    }

    getCat(): Promise<Cat[]> {
        const cats = this.catRepository.find().exec();
        return cats;
    }

    addCat(cat: Cat): Promise<Cat> {
        const createdCat = new this.catRepository(cat);
        return createdCat.save();
    }

    updateCat(cat: Cat) {
        return this.catRepository.updateOne({ _id: cat._id }, cat);
    }

    deleteCat(id: string) {
        return this.catRepository.deleteOne({ _id: id });
    }
}
