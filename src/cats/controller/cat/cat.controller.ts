import { Controller, Get, Post, Body, Put, Delete, Param, ParseIntPipe, HttpStatus } from '@nestjs/common';
import { CatService } from 'src/cats/services/cat/cat.service';
import { Cat } from 'src/cats/schema/cat.schema';
// import { Cat } from 'src/cats/models/cat.model';

@Controller('cat')
export class CatController {

    constructor(private catService: CatService) { }

    @Get()
    getCat() {
        return this.catService.getCat();
    }

    @Post()
    addCat(@Body() cat: Cat) {
        return this.catService.addCat(cat);
    }

    @Put()
    updateCat(@Body() cat: Cat) {
        return this.catService.updateCat(cat);
    }

    @Delete(':id')
    deleteCat(@Param('id') id: string) {
        return this.catService.deleteCat(id);
    }

    // @Delete(':id')
    // deleteCat(@Param('id', new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: number) {
    //     return this.catService.deleteCat(id);
    // }
}
