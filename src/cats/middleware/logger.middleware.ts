import { Injectable, NestMiddleware, HttpException, HttpStatus, UseFilters } from '@nestjs/common';
import { Request, Response } from 'express';
import { HttpExceptionFilter } from 'src/cats/exception/http-exception-filter.exception';

@Injectable()
@UseFilters(new HttpExceptionFilter())
export class LoggerMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: Function) {
    console.log('Request...');
    // new HttpExceptionFilter();
    throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
    next();
  }
}
