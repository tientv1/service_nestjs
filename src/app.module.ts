import { Module, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsModule } from './cats/cats.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductModule } from './product/product.module';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { SocketModule } from './socket/socket.module';
import { AuthGuard } from 'src/auth/auth.guard';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { AuthMiddleWare } from 'src/middleware/auth.middleware';   


@Module({
  imports: [
    // MongooseModule.forRoot('mongodb://localhost:27017/testnestjs', { useNewUrlParser: true }),
    MongooseModule.forRoot('mongodb+srv://testnestjs:Viettien06@cluster0.ye7dx.mongodb.net/trainingnestjs?retryWrites=true&w=majority', { useNewUrlParser: true, useFindAndModify: false }),
    // CatsModule,
    ProductModule,
    // SocketModule,
    UsersModule,
    AuthModule,

  ],
  controllers: [AppController],
  providers: [
    AppService
  ],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleWare)
      .forRoutes({ path: 'users', method: RequestMethod.GET });
  }
}
