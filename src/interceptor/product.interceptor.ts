import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class ProductInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        return next
            .handle()
            .pipe(map(data => {
                if (Array.isArray(data)) {
                    return data.map(product => {
                        return {
                            id: product._id,
                            name: product.name,
                            type: product.type,
                            trademark: product.trademark,
                            price: product.price
                        }
                    })
                }
                return {
                    id: data._id,
                    name: data.name,
                    type: data.type,
                    trademark: data.trademark,
                    price: data.price
                }
            }));
    }
}