import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class UserInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        return next
            .handle()
            .pipe(map(data => {
                if (Array.isArray(data)) {
                    return data.map(user => {
                        return {
                            id: user._id,
                            fullname: user.fullname,
                            gender: user.gender,
                            birthday: user.birthday,
                            email: user.email,
                            phoneNumberPrefix: user.phoneNumberPrefix,
                            phoneNumber: user.phoneNumber,
                            status: user.status
                            // username: user.username,
                            // password: user.password,
                            // role: user.role
                        }
                    })
                }
                if (typeof (data) === 'object') {
                    return {
                        id: data._id,
                        fullname: data.fullname,
                        gender: data.gender,
                        birthday: data.birthday,
                        email: data.email,
                        phoneNumberPrefix: data.phoneNumberPrefix,
                        phoneNumber: data.phoneNumber,
                        status: data.status
                        // username: data.username,
                        // password: data.password,
                        // role: data.role
                    }
                }
                return data;
            }));
    }
}