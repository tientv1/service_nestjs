import { User } from 'src/users/schema/user.schema';

export interface UserSchema {
    states: {
        pending: {},
        change_status: {},
        active_processing: {},
        inactive_processing: {},
        final: {},
        error: {}
    };
}

export interface UserContext {
    user: any;
}