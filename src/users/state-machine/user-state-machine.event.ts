import { EventObject } from 'xstate';
import { User } from 'src/users/schema/user.schema';

export class ChangeStatus implements EventObject {
    static readonly type = '[USER] Change Status';
    readonly type = ChangeStatus.type;
    constructor(public user: User) { }
}

export class ActiveUser implements EventObject {
    static readonly type = '[USER] Active User';
    readonly type = ActiveUser.type;
    constructor(public userId: String) { }
}

export class InactiveUser implements EventObject {
    static readonly type = '[USER] Inactive User';
    readonly type = InactiveUser.type;
    constructor(public userId: String) { }
}

export type UserEvent = ChangeStatus | ActiveUser | InactiveUser;