import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { MachineOptions, AnyEventObject, Machine, interpret, assign } from 'xstate';
import { UserEvent, ActiveUser, InactiveUser, ChangeStatus } from 'src/users/state-machine/user-state-machine.event';
import { UserContext, UserSchema } from 'src/users/state-machine/user-state-machine.schema';
import { UserMachineConfig } from 'src/users/state-machine/user-state-machine.config';
import { from } from 'rxjs';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ActiveUserCommand, InactiveUserCommand } from 'src/users/command/user.command';
import { FindOneUserQuery } from 'src/users/query/user.query';

@Injectable()
export class UserStateMachineService {
    private machine;
    private service;

    constructor(
        private commandBus: CommandBus,
        private queryBus: QueryBus
    ) { }

    private userStateMachineOptions: Partial<MachineOptions<UserContext, UserEvent>> = {
        actions: {
            assignUser: assign<UserContext, UserEvent>((_, event: ChangeStatus) => {
                return {
                    user: event.user
                }
            }),
            logMessage: (_, event: AnyEventObject) => {
                console.log(event.msg)
            },
            activeUser: (_, event: AnyEventObject) => {
                console.log('active user success')
            },
            inactiveUser: (_, event: AnyEventObject) => {
                console.log('inactive user success')
            },
            showError: (_, event: AnyEventObject) => {
                console.log('error');
                throw new BadRequestException();
            }
        },
        services: {
            activeUser: (_, event: ActiveUser) => this.commandBus.execute(new ActiveUserCommand(event.userId)),
            inactiveUser: (_, event: InactiveUser) => this.commandBus.execute(new InactiveUserCommand(event.userId)),
        },
        guards: {
            isInactive: (context, event) => context.user && context.user.status === 'inactive',
            isActive: (context, event) => context.user && context.user.status === 'active'
        }
    }

    initialStateMachine() {
        console.log('start')
        this.machine = Machine<UserContext, UserSchema, UserEvent>(UserMachineConfig)
            .withConfig(this.userStateMachineOptions);
        this.service = interpret(this.machine)
            .start();

        const state$ = from(this.service);
        state$.subscribe((state: any) => {
            console.log(state.value);
            // console.log(`---------------------`);
            // // console.log(`State changed the ${this.cnt++} time!`);
            // if (!!state.transitions[0]) {
            //     console.log(`Event: ${state.transitions[0].event} has just been sent!`);
            // }
            // console.log(`Current state: ${state.value}`);
            // console.log(`Available events: ${state.nextEvents}`);
            // console.log(state.value);
            // console.log(`---------------------`);
        });
    }

    transition(event: UserEvent) {
        this.service.send(event);
    }

    async transittionToChangeStatus(userId: String) {
        const user = await this.queryBus.execute(new FindOneUserQuery(userId));
        if (!user) {
            throw new NotFoundException();
        }
        this.transition(new ChangeStatus(user));
    }

    async activeUser(userId: String) {
        await this.transittionToChangeStatus(userId);
        await this.transition(new ActiveUser(userId));
    }

    async inactiveUser(userId: String) {
        await this.transittionToChangeStatus(userId);
        await this.transition(new InactiveUser(userId));
    }
}