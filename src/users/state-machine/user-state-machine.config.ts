import { UserContext, UserSchema } from './user-state-machine.schema';
import { MachineConfig } from 'xstate';
import {
    UserEvent, ActiveUser, InactiveUser, ChangeStatus
} from './user-state-machine.event';

export const context: UserContext = {
    user: {}
};

export const UserMachineConfig: MachineConfig<UserContext, UserSchema, UserEvent> = {
    id: 'state_machine_user',
    initial: 'pending',
    context,
    states: {
        pending: {
            on: {
                [ChangeStatus.type]: {
                    target: 'change_status',
                    actions: 'assignUser'
                }
            }
        },
        change_status: {
            on: {
                [ActiveUser.type]: [
                    {
                        target: 'active_processing',
                        cond: 'isInactive'
                    },
                    {
                        target: 'error',
                        cond: 'isActive'
                    }
                ],
                [InactiveUser.type]: [
                    {
                        target: 'inactive_processing',
                        cond: 'isActive'
                    },
                    {
                        target: 'error',
                        cond: 'isInactive'
                    }
                ]
            }
        },
        active_processing: {
            invoke: {
                id: 'activeUser',
                src: 'activeUser',
                onDone: {
                    target: 'final',
                    actions: ['activeUser', 'logMessage']
                },
                onError: {
                    target: 'error',
                    actions: ['logMessage']
                }
            }
        },
        inactive_processing: {
            invoke: {
                id: 'inactiveUser',
                src: 'inactiveUser',
                onDone: {
                    target: 'final',
                    actions: ['inactiveUser', 'logMessage']
                },
                onError: {
                    target: 'error',
                    actions: ['logMessage']
                }
            }
        },
        final: {
            type: 'final'
        },
        error: {
            always: {
                target: 'final',
                actions: ['showError']
            }
        }
    }
};
