import { User } from 'src/users/schema/user.schema';

export class AddUserCommand {
    constructor(
        public readonly user: User
    ) { }
}

export class UpdateUserCommand {
    constructor(
        public readonly userId: String,
        public readonly user: User
    ) { }
}

export class DeleteUserCommand {
    constructor(
        public readonly userId: String
    ) { }
}

export class ActiveUserCommand {
    constructor(
        public readonly userId: String
    ) { }
}

export class InactiveUserCommand {
    constructor(
        public readonly userId: String
    ) { }
}