import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from 'src/users/schema/user.schema';
import { CqrsModule } from '@nestjs/cqrs';
import { AddUserHandler } from 'src/users/handler/command-handler/add-user.handler';
import { UpdateUserHandler } from 'src/users/handler/command-handler/update-user.handler';
import { FindUserByUsernameHandler } from 'src/users/handler/query-handler/find-user-by-username.handler';
import { FindAllUserHandler } from 'src/users/handler/query-handler/find-all-user.handler';
import { FindOneUserHandler } from 'src/users/handler/query-handler/find-one-user.handler';
import { DeleteUserHandler } from 'src/users/handler/command-handler/delete-user.handler';
import { UserQueryController } from 'src/users/controller/user-query.controller';
import { UserCommandController } from 'src/users/controller/user-command.controller';
import { UserStateMachineService } from 'src/users/state-machine/user-state-machine.service';
import { ActiveUserHandler } from 'src/users/handler/command-handler/active-user.handler';
import { InactiveUserHandler } from 'src/users/handler/command-handler/inactive-user.handler';

const CommandHandler = [AddUserHandler, UpdateUserHandler, DeleteUserHandler, ActiveUserHandler, InactiveUserHandler]
const QueryHandler = [FindUserByUsernameHandler, FindAllUserHandler, FindOneUserHandler]

@Module({
    imports: [
        MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
        CqrsModule
    ],
    controllers: [
        UserQueryController,
        UserCommandController
    ],
    providers: [
        UserStateMachineService,
        ...CommandHandler,
        ...QueryHandler
    ],
    exports: [
        ...CommandHandler,
        ...QueryHandler
    ]
})
export class UsersModule {
}
