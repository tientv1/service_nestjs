import { Controller, Post, Body, UseInterceptors, Param, Put, HttpException, HttpStatus, Delete, Patch, UseGuards, NotFoundException } from '@nestjs/common';
import { User } from 'src/users/schema/user.schema';
import { ApiTags } from '@nestjs/swagger';
import { UserInterceptor } from 'src/interceptor/user.interceptor';
import { CommandBus } from '@nestjs/cqrs';
import { AddUserCommand, UpdateUserCommand, DeleteUserCommand } from 'src/users/command/user.command';
import { UserStateMachineService } from 'src/users/state-machine/user-state-machine.service';

@Controller('user')
@ApiTags('User')
@UseInterceptors(UserInterceptor)
export class UserCommandController {

    constructor(
        private commandBus: CommandBus,
        private userStateMachineService: UserStateMachineService
    ) { }

    @Post()
    async createUser(@Body() user: User) {
        return this.commandBus.execute(
            new AddUserCommand(user)
        ).catch(err => {
            throw new HttpException(err, HttpStatus.BAD_REQUEST)
        });
    }

    @Put(':id')
    async updateUser(@Param('id') id: String, @Body() user: User) {
        return this.commandBus.execute(
            new UpdateUserCommand(id, user)
        ).catch(err => {
            throw new HttpException(err, HttpStatus.BAD_REQUEST)
        });
    }

    @Patch(':id/active')
    async activeUser(@Param('id') id: String) {
        await this.userStateMachineService.initialStateMachine();
        return this.userStateMachineService.activeUser(id);
    }

    @Patch(':id/inactive')
    inactiveUser(@Param('id') id: String) {
        this.userStateMachineService.initialStateMachine();
        return this.userStateMachineService.inactiveUser(id);
    }

    @Delete(':id')
    async deleteUser(@Param('id') id: String) {
        return this.commandBus.execute(
            new DeleteUserCommand(id)
        ).catch(err => {
            throw new HttpException(err, HttpStatus.INTERNAL_SERVER_ERROR)
        });
    }
}