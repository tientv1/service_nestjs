import { Controller, Get, UseInterceptors, Param, HttpException, HttpStatus } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { UserInterceptor } from 'src/interceptor/user.interceptor';
import { QueryBus } from '@nestjs/cqrs';
import { FindOneUserQuery, FindAllUserQuery } from 'src/users/query/user.query';

@Controller('user')
@ApiTags('User')
@UseInterceptors(UserInterceptor)
export class UserQueryController {

    constructor(
        private queryBus: QueryBus
    ) { }

    @Get()
    async findAll() {
        return this.queryBus.execute(
            new FindAllUserQuery()
        ).catch(err => {
            throw new HttpException(err, HttpStatus.INTERNAL_SERVER_ERROR)
        });;
    }

    @Get(':id')
    async getOne(@Param('id') id: String) {
        return await this.queryBus.execute(
            new FindOneUserQuery(id)
        ).catch(err => {
            throw new HttpException(err, HttpStatus.NOT_FOUND)
        });;
    }

}