export class FindAllUserQuery {
    constructor() { }
}

export class FindOneUserQuery {
    constructor(public readonly id: String) { }
}

export class FindUserByUsernameQuery {
    constructor(public readonly username: String) { }
}