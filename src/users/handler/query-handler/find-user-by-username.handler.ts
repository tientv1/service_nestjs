import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { FindUserByUsernameQuery } from 'src/users/query/user.query';
import { InjectModel } from '@nestjs/mongoose';
import { User } from 'src/users/schema/user.schema';
import { Model } from 'mongoose';

@QueryHandler(FindUserByUsernameQuery)
export class FindUserByUsernameHandler implements IQueryHandler<FindUserByUsernameQuery>{

    constructor(@InjectModel(User.name) private readonly userRepository: Model<User>) { }

    execute(query: FindUserByUsernameQuery): Promise<User> {
        const username = query.username;
        return this.userRepository.findOne({ username: username }).exec();
    }

}