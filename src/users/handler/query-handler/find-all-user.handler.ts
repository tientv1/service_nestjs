import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { FindAllUserQuery } from 'src/users/query/user.query';
import { User } from 'src/users/schema/user.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@QueryHandler(FindAllUserQuery)
export class FindAllUserHandler implements IQueryHandler<FindAllUserQuery>{
    constructor(@InjectModel(User.name) private readonly userRepository: Model<User>) { }

    execute(query: FindAllUserQuery): Promise<User[]> {
        return this.userRepository.find().exec();
    }

}