import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { FindOneUserQuery } from 'src/users/query/user.query';
import { InjectModel } from '@nestjs/mongoose';
import { User } from 'src/users/schema/user.schema';
import { Model } from 'mongoose';
import { NotFoundException } from '@nestjs/common';

@QueryHandler(FindOneUserQuery)
export class FindOneUserHandler implements IQueryHandler<FindOneUserQuery>{

    constructor(@InjectModel(User.name) private readonly userRepository: Model<User>) { }

    async execute(query: FindOneUserQuery): Promise<User> {
        const userId = query.id;
        const checkUser = await this.userRepository.exists({ _id: userId }).catch(err => {
            throw new NotFoundException();
        });
        if (await !checkUser) {
            throw new NotFoundException();
        }
        return await this.userRepository.findById({ _id: userId }).exec();
    }

}