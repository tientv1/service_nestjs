import { ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { AddUserCommand } from 'src/users/command/user.command';
import { InjectModel } from '@nestjs/mongoose';
import { User } from 'src/users/schema/user.schema';
import { Model } from 'mongoose';

@CommandHandler(AddUserCommand)
export class AddUserHandler implements ICommandHandler<AddUserCommand>{

    constructor(@InjectModel(User.name) private readonly userRepository: Model<User>) { }

    execute(command: AddUserCommand): Promise<any> {
        const user = command.user;
        user.status = 'active';
        const u = new this.userRepository(user);
        return u.save();
    }
}