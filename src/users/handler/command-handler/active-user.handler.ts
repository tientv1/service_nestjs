import { ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { ActiveUserCommand } from 'src/users/command/user.command';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from 'src/users/schema/user.schema';


@CommandHandler(ActiveUserCommand)
export class ActiveUserHandler implements ICommandHandler<ActiveUserCommand>{

    constructor(@InjectModel(User.name) private readonly userRepository: Model<User>) { }

    async execute(command: ActiveUserCommand): Promise<any> {
        const userId = command.userId;
        return this.userRepository.findByIdAndUpdate(userId, { status: 'active' }, { new: true }).exec();
    }

}