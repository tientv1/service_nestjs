import { ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { DeleteUserCommand } from 'src/users/command/user.command';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from 'src/users/schema/user.schema';
import { NotFoundException } from '@nestjs/common';

@CommandHandler(DeleteUserCommand)
export class DeleteUserHandler implements ICommandHandler<DeleteUserCommand>{

    constructor(@InjectModel(User.name) private readonly userRepository: Model<User>) { }

    async execute(command: DeleteUserCommand): Promise<any> {
        const userId = command.userId;
        const checkUser = await this.userRepository.exists({ _id: userId }).catch(err => {
            throw new NotFoundException();
        });
        if (await !checkUser) {
            throw new NotFoundException();
        }
        return this.userRepository.findByIdAndDelete(userId);
    }

}