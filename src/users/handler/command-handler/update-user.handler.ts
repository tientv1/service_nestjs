import { ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { UpdateUserCommand } from 'src/users/command/user.command';
import { InjectModel } from '@nestjs/mongoose';
import { User } from 'src/users/schema/user.schema';
import { Model } from 'mongoose';
import { NotFoundException } from '@nestjs/common';

@CommandHandler(UpdateUserCommand)
export class UpdateUserHandler implements ICommandHandler<UpdateUserCommand>{

    constructor(@InjectModel(User.name) private readonly userRepository: Model<User>) { }

    async execute(command: UpdateUserCommand): Promise<User> {
        const userId = command.userId;
        const user = command.user;
        const checkUser = await this.userRepository.exists({ _id: userId }).catch(err => {
            throw new NotFoundException();
        });
        if (await !checkUser) {
            throw new NotFoundException();
        }
        return await this.userRepository.findByIdAndUpdate(userId, user, { new: true }).exec();
    }
}