import { ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { InactiveUserCommand } from 'src/users/command/user.command';
import { NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from 'src/users/schema/user.schema';


@CommandHandler(InactiveUserCommand)
export class InactiveUserHandler implements ICommandHandler<InactiveUserCommand>{

    constructor(@InjectModel(User.name) private readonly userRepository: Model<User>) { }

    async execute(command: InactiveUserCommand): Promise<any> {
        const userId = command.userId;
        return this.userRepository.findByIdAndUpdate(userId, { status: 'inactive' }, { new: true }).exec();
    }

}