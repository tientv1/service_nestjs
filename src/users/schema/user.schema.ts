import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { RolesEnum } from 'src/shares/enum/role.enum';
import { Document } from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';

@Schema()
export class User extends Document {

    @ApiProperty()
    @Prop()
    fullname: String;

    @ApiProperty()
    @Prop()
    birthday: Date;

    @ApiProperty()
    @Prop()
    gender: String;

    @ApiProperty()
    @Prop()
    email: String;

    @ApiProperty()
    @Prop()
    phoneNumberPrefix: String;

    @ApiProperty()
    @Prop()
    phoneNumber: String;

    // @ApiProperty()
    // @Prop()
    // username: String;

    // @ApiProperty()
    // @Prop()
    // password: String;

    // @ApiProperty()
    // @Prop()
    // role: RolesEnum;

    @ApiProperty()
    @Prop()
    status: String;
}

export const UserSchema = SchemaFactory.createForClass(User);