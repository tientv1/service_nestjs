import { Injectable } from '@nestjs/common';
import { AuthDTO } from 'src/auth/auth.dto';
import { JwtService } from '@nestjs/jwt';
import { QueryBus } from '@nestjs/cqrs';
import { FindUserByUsernameQuery } from 'src/users/query/user.query';

@Injectable()
export class AuthService {
    constructor(
        private jwtService: JwtService,
        private queryBus: QueryBus
    ) { }

    async validateUser(auth: AuthDTO): Promise<any> {
        const user = await this.queryBus.execute(
            new FindUserByUsernameQuery(auth.username)
        )
        if (user && user.password === auth.password) {
            const { password, ...result } = user;
            return result;
        }
        return null;
    }

    async login(user) {
        const payload = { username: user.username, role: user.role, key: 'demonestjs' };
        // console.log(this.jwtService.decode('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwicm9sZSI6ImFkbWluIiwia2V5IjoiZGVtb25lc3RqcyIsImlhdCI6MTU5NDI2NTQ0NiwiZXhwIjoxNTk0MjY1NTA2fQ.hzuNZ0WhLO5JY0n8VD4bMfWe1PjC7qJSGeEnWZN9hpE'));
        return {
            access_token: this.jwtService.sign(payload)
        };
    }
}