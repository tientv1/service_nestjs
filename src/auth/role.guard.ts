import { Injectable, ExecutionContext } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class RoleAuthGuard extends AuthGuard('role') {

    constructor(private jwtService: JwtService) {
        super();
    }

    async canActivate(context: ExecutionContext) {
        const request = await context.switchToHttp().getRequest();
        const jwt = await request.headers.authorization;
        const index = await jwt.indexOf(' ');
        const claim = await jwt.slice(index + 1);
        console.log(claim);
        const role = await this.jwtService.decode(claim);
        console.log(role);
        return true;
    }
}