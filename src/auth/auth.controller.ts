import { Controller, Post, Request, Body, HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { AuthDTO } from 'src/auth/auth.dto';
import { AuthService } from 'src/auth/auth.service';
import { ApiTags } from '@nestjs/swagger';
import { LocalAuthGuard } from 'src/auth/local-auth.guard';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) { }

    @UseGuards(LocalAuthGuard)
    @Post('login')
    async login(@Body() auth: AuthDTO, @Request() req) {
        return this.authService.login(req.user._doc);
    }
}