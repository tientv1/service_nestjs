import { Injectable, CanActivate, ExecutionContext, HttpException, HttpStatus } from '@nestjs/common';
// import { AuthService } from 'src/auth/auth.service';
// import { RolesEnum } from 'src/shares/enum/role.enum';

@Injectable()
export class AuthGuard implements CanActivate {

    // constructor(private authService: AuthService) { }

    canActivate(context: ExecutionContext) {
        const request = context.switchToHttp().getRequest();
        if (!request.headers.authorization) {
            return false;
        }
        this.checkRole(request.headers.authorization);
        return true;
    }

    async checkRole(jwt: string) {
        const index = await jwt.indexOf(' ');
        const string = await jwt.slice(index + 1);
        // const role = await this.authService.getRole(string);
        // console.log(role);
        // if (await role !== RolesEnum.Admin) {
        //     return false;
        // }
        return true;
    }
}