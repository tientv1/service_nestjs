import { ApiProperty } from '@nestjs/swagger';

export class AuthDTO {
    @ApiProperty()
    username: String;

    @ApiProperty()
    password: String;
}