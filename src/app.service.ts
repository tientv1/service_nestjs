import { Injectable } from '@nestjs/common';
import { ClientProxy, ClientProxyFactory, Transport } from '@nestjs/microservices';
import { from } from 'rxjs';

@Injectable()
export class AppService {
  private client: ClientProxy;

  constructor() {
    // TCP
    // this.client = ClientProxyFactory.create({
    //   transport: Transport.TCP,
    //   options: {
    //     host: '127.0.0.1',
    //     port: 8877
    //   }
    // })

    // redis
    // this.client = ClientProxyFactory.create({
    //   transport: Transport.REDIS,
    //   options: {
    //     url: 'redis://localhost:6379',
    //   }
    // })

    //MQTT
    // this.client = ClientProxyFactory.create({
    //   transport: Transport.MQTT,
    //   options: {
    //     url: 'mqtt://localhost:1883',
    //   }
    // })

    // RabbitMQ
    this.client = ClientProxyFactory.create({
      transport: Transport.RMQ,
      options: {
        urls: ['amqp://npjkjofm:ZrVB-ttgck8bda_kLMsZUkzY1CDOeY6K@emu.rmq.cloudamqp.com/npjkjofm'],
        queue: 'readname',
        queueOptions: {
          durable: false
        }
      }
    })

  }

  getHello(msg: string) {
    return this.client.send<string, string>('read', msg);
  }
}
