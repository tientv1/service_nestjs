import { Injectable } from '@nestjs/common';
import { Product } from 'src/product/schema/product.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Guid } from "guid-typescript";

@Injectable()
export class ProductCommandService {

    constructor(@InjectModel(Product.name) private readonly productRepository: Model<Product>) {
    }

    async addProduct(product: Product): Promise<Product> {
        const productModel = await new this.productRepository(product);
        return productModel.save();
    }

    async updateProduct(product: Product, id: string): Promise<Product> {
        // return this.productRepository.findByIdAndUpdate({ _id: id }, product, { new: true });
        return await this.productRepository.updateOne({ _id: id }, product);
    }

    async deleteProduct(id: string) {
        return await this.productRepository.findByIdAndDelete({ _id: id });
        // return this.productRepository.deleteOne({ _id: id });
    }
}