import { Injectable } from '@nestjs/common';
import { Product } from 'src/product/schema/product.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class ProductQueryService {

    constructor(@InjectModel(Product.name) private readonly productRepository: Model<Product>) {
    }

    async findAll(): Promise<Product[]> {
        return await this.productRepository.find().exec();
    }

    async findById(id: string): Promise<Product> {
        return await this.productRepository.findById(id);
    }

}