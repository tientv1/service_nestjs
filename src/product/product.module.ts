import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Product, ProductSchema } from 'src/product/schema/product.schema';
import { ProductQueryController } from 'src/product/controller/product-query.controller';
import { ProductCommandController } from 'src/product/controller/product-command.controller';
import { ProductCommandService } from 'src/product/service/product-command.service';
import { ProductQueryService } from 'src/product/service/product-query.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Product.name, schema: ProductSchema }])
  ],
  controllers: [
    ProductCommandController,
    ProductQueryController
  ],
  providers: [
    ProductCommandService,
    ProductQueryService
  ]
})
export class ProductModule { }
