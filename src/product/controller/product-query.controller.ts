import { Controller, Get, Param, UseInterceptors } from '@nestjs/common';
import { ApiTags, ApiParam } from '@nestjs/swagger';
import { ProductQueryService } from 'src/product/service/product-query.service';
import { ProductInterceptor } from 'src/interceptor/product.interceptor';

@ApiTags('Product')
@Controller('product')
@UseInterceptors(ProductInterceptor)
export class ProductQueryController {

    constructor(private productQueryService: ProductQueryService) { }

    @Get()
    findAll() {
        return this.productQueryService.findAll();
    }

    @Get(':id')
    @ApiParam({ name: 'id' })
    findById(@Param('id') id: string) {
        return this.productQueryService.findById(id);
    }

}
