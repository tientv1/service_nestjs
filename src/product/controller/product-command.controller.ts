import { Controller, Post, Body, Put, Delete, Param, HttpException, HttpStatus, UseInterceptors } from '@nestjs/common';
import { Product } from 'src/product/schema/product.schema';
import { ApiTags, ApiParam } from '@nestjs/swagger';
import { ProductCommandService } from 'src/product/service/product-command.service';
import { ProductInterceptor } from 'src/interceptor/product.interceptor';

@ApiTags('Product')
@Controller('product')
@UseInterceptors(ProductInterceptor)
export class ProductCommandController {

    constructor(private productCommandService: ProductCommandService) { }

    @Post()
    addProduct(@Body() product: Product) {
        try {
            return this.productCommandService.addProduct(product);
        }
        catch (err) {
            return new HttpException(err.message, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Put(':id')
    @ApiParam({ name: 'id' })
    updateProduct(@Body() product: Product, @Param('id') id: string) {
        return this.productCommandService.updateProduct(product, id);
    }

    @Delete(':id')
    @ApiParam({ name: 'id' })
    deleteProduct(@Param('id') id: string) {
        return this.productCommandService.deleteProduct(id);
    }
}
