import { ApiProperty } from '@nestjs/swagger';

export class Type {
    @ApiProperty()
    name: string;
}