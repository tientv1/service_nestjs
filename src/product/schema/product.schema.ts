import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Type } from 'src/product/schema/type.schema';
import { ApiProperty } from '@nestjs/swagger';

@Schema()
export class Product extends Document {

    @ApiProperty({
        type: String,
    })
    @Prop()
    name: {
        type: string,
        required: [true, 'Please EnterProduct Name']
    };

    @ApiProperty({
        type: String,
    })
    @Prop()
    type: {
        type: string,
        required: [true, 'Please EnterProduct type']
    };

    @ApiProperty({
        type: String,
    })
    @Prop()
    trademark: {
        type: string,
        required: [true, 'Please EnterProduct trademark']
    };

    @ApiProperty({
        type: String,
    })
    @Prop()
    price: {
        type: number,
        required: [true, 'Please EnterProduct price']
    };
}

export const ProductSchema = SchemaFactory.createForClass(Product);